import axios from 'axios'

class Files {

    uploadListingsFile(data){
        const url = 'http://localhost:5000/uploadListings';
        return axios.post(url, data);
    }

    uploadContactsFile(data){
        const url = 'http://localhost:5000/uploadContacts';
        return axios.post(url, data);
    }

    deleteAll(){
        const url  = 'http://localhost:5000/deleteAll';
        return axios.get(url);
    }

}

export default new Files();