import axios from 'axios'


class ListingsContacts {
     
    getAveragePriceOfThirtyPercentMostContactedListings(){
        const url = 'http://localhost:5000/averagePrice';
        return axios.get(url);
    }


    getAverageSellingPricePerSeller(){
        const url = 'http://localhost:5000/averageSellingPrice';
        return axios.get(url);
    }

    getTopFiveMostContactedListingPerMonth(){
        const url = 'http://localhost:5000/topFiveMostContacted';
        return axios.get(url);
    }

    getMakePercent(){
        const url  = 'http://localhost:5000/getMakePercent';
        return axios.get(url);
    }

}

export default new ListingsContacts();