import React, {Component} from 'react';

import {Card, Table} from 'react-bootstrap';
import ListingsContacts from '../services/ListingsContacts';

export default class AveragePriceMostContactedListing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            averagePrice : ''
        };
    }

    componentDidMount() {
        ListingsContacts
        .getAveragePriceOfThirtyPercentMostContactedListings()
        .then((response) => {
               this.setState({ averagePrice: response.data})
          });
     }

    render() {
        return (
          <Card className={"border border-dark bg-dark text-white"}>
            <h4 align="center"> Average Price of the 30% Most Contacted Listings</h4>
            <Card.Body>
                <Table bordered hover striped variant="dark">   
                    <thead>
                        <tr>                           
                          <th>Average Price</th>
                       </tr>
                   </thead>
                   <tbody>
                        {                                   
                           <tr>
                            <td>€ {this.state.averagePrice},-</td>
                           </tr>
                        }                               
                   </tbody>
                </Table>
            </Card.Body>
          </Card>
              );
            }
}

