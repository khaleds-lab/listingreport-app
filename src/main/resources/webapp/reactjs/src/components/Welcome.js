import React, {Component} from 'react';

import {Jumbotron} from 'react-bootstrap';

export default class Welcome extends Component {
    render() {
        return (
            <Jumbotron className="bg-dark text-white">
                <h1>Listing Report</h1>
                <blockquote className="blockquote mb-0">
                    <p>
                        An application to retrive reports based off of CSV listing files
                    </p>
                    <footer className="blockquote-footer">
                        Khaled Ahmed
                    </footer>
                </blockquote>
            </Jumbotron>
        );
    }
}