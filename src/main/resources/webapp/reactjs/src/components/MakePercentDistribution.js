import React, {Component} from 'react';

import {Card, Table} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faList} from '@fortawesome/free-solid-svg-icons';
import ListingsContacts from '../services/ListingsContacts';


export default class MakePercentDistribution extends Component {

    constructor(props) {
        super(props);
        this.state = {
            makePercent : []
        };
    }

    componentDidMount() {
        ListingsContacts.getMakePercent().then((response) => {
               this.setState({ makePercent: response.data})
          });
     }

    render() {
        return (            
            <Card className={"border border-dark bg-dark text-white"}>
                <h4 align="center"> Average Listing Selling Price per Seller Type</h4>
                <Card.Header><FontAwesomeIcon icon={faList} /> </Card.Header>
                    <Card.Body>
                        <Table bordered hover striped variant="dark">
                            <thead>
                                <tr>
                                    <th>Seller Type</th>
                                    <th>Average price</th>                                              
                                </tr>
                            </thead>
                            <tbody>
                                {
                                this.state.makePercent.map( make =>
                                <tr>                                                    
                                    <td>{make.make}</td>
                                    <td> {make.percentage}%</td>                                                    
                                </tr>
                                        )
                                }
                                </tbody>
                            </Table>
                </Card.Body>
            </Card>                                                              
                    );
            }
}

