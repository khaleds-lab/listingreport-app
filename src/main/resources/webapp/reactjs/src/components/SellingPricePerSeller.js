import React, {Component} from 'react';

import {Card, Table} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faList} from '@fortawesome/free-solid-svg-icons';
import ListingsContacts from '../services/ListingsContacts';


export default class SellingPricePerSeller extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sellingPricePerSeller : []
        };
    }

    componentDidMount() {
        ListingsContacts.getAverageSellingPricePerSeller().then((response) => {
               this.setState({ sellingPricePerSeller: response.data})
          });
     }

    render() {
        return (            
            <Card className={"border border-dark bg-dark text-white"}>
                <h4 align="center"> Average Listing Selling Price per Seller Type</h4>
                <Card.Header><FontAwesomeIcon icon={faList} /></Card.Header>
                    <Card.Body>
                        <Table bordered hover striped variant="dark">
                            <thead>
                                <tr>
                                    <th>Seller Type</th>
                                    <th>Average price</th>                                              
                                </tr>
                            </thead>
                            <tbody>
                                {
                                 this.state.sellingPricePerSeller.map( seller =>
                                    <tr>                                                    
                                        <td>{seller.sellerType}</td>
                                        <td>€ {seller.sellerPrice},-</td>                                                    
                                    </tr>
                                    )
                                }
                            </tbody>
                         </Table>
                    </Card.Body>
                        </Card>                 
                    );
            }
}

