
import React,{Component} from 'react';
import Files from '../services/Files';

export default class FileAction extends Component {

	state = {
	listingsFile: null,
	contactsFile: null
	};
	
	onListingsChange = event => {
	this.setState({ listingsFile: event.target.files[0] });
	};

	onContactsChange = event => {
		this.setState({ contactsFile: event.target.files[0] });
	};

	onFileUpload = () => {

	const listingData = new FormData();
	listingData.append("file" ,this.state.listingsFile);
	Files.uploadListingsFile(listingData)
	.then(res => {
		console.log(res.data);
		alert("Listings uploaded successfully.")
				 }	)

	const contactsData = new FormData();
	contactsData.append("file" ,this.state.contactsFile);
	Files.uploadContactsFile(contactsData)
	.then(res => {
		console.log(res.data);
		alert("Contacts uploaded successfully.")
				 }	)
	};

	onDelete = () => {
	Files.deleteAll()
	.then(res => {
		console.log(res.data);
		alert("Files Deleted successfully.")
					}	)
	};
	
	fileData = () => {	
	if (this.state.listingsFile && this.state.contactsFile) {		
		return (
		<div>
		<h4>File Details:</h4>			
		<p>File Name: {this.state.listingsFile.name}</p>
		<p>File Type: {this.state.listingsFile.type}</p>			
		<p>Last Modified:{" "}{this.state.listingsFile.lastModifiedDate.toDateString()}</p>
		<br></br>
		<p>File Name: {this.state.contactsFile.name}</p>
		<p>File Type: {this.state.contactsFile.type}</p>			
		<p>Last Modified:{" "}{this.state.contactsFile.lastModifiedDate.toDateString()}</p>
		</div>
		);
	} else {
		return (
		<div>
			<br />
			<h4>Please Upload both Listings and Contacts Files</h4>
		</div>
		);
	}
	};

	render() {
	return (
		<div>	
			<div>
				<h3> Delete Current CSV files</h3>
				<button onClick={this.onDelete}> Delete All</button>
			</div>	
			<br></br>
			<div>
				<h3> Upload Custom CSV files</h3>
				<label>LISTINGS =={'>'}</label>
				<input type="file" onChange={this.onListingsChange}/>
				<br></br>
				<label>Contacts =={'>'}</label>
				<input type="file" onChange={this.onContactsChange} />	
				<br></br>	
				<br></br>		
				<button onClick={this.onFileUpload}>Upload Files!</button>
			</div>
			<br></br>
			{this.fileData()}
		</div>
		);
	}
}