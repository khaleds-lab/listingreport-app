import React, {Component} from 'react';

import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default class NavigationBar extends Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Link to={""} className="navbar-brand">Home</Link>
                <Nav>
                    <Link to={"averageSellingPrice"} className="nav-link"> SellingPricePerSeller</Link>
                    <Link to={"getMakePercent"} className="nav-link">MakePercentDistribution</Link>
                    <Link to={"averagePrice"} className="nav-link">AveragePrice</Link>
                    <Link to={"topFiveMostContacted"} className="nav-link">TopFiveMostContacted</Link>
                    <Link to={"fileUpload"} className="nav-link">FileAction</Link>
                </Nav>
            </Navbar>
        );
    }
}