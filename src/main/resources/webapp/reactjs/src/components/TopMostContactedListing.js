import React, {Component} from 'react';

import {Card, Table} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faList} from '@fortawesome/free-solid-svg-icons';
import ListingsContacts from '../services/ListingsContacts';


export default class TopMostContactedListing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            topContactedListings : []
        };
    }

    componentDidMount() {
        ListingsContacts.getTopFiveMostContactedListingPerMonth().then((response) => {
               this.setState({ topContactedListings: response.data})
          });
     }

    render() {
        
        let month = 0;
        let rank = 0;
        return (
             this.state.topContactedListings.map(list =>
                <tr key={list.size}>
                     <Card className={"border border-dark bg-dark text-white"}>
                            <Card.Header><FontAwesomeIcon icon={faList} /> Month:  {++month}.2020</Card.Header>
                                    <Card.Body>
                                        <Table bordered hover striped variant="dark">
                                            <thead>
                                                <tr>
                                                  <th>Ranking</th>
                                                  <th>Listing Id</th>
                                                  <th>Make</th>
                                                  <th>Selling Price</th>
                                                  <th>Mileage</th>
                                                  <th>Total amount of contacts</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                   {
                                                    list.map(
                                                    listingContact =>
                                                    <tr key={listingContact.size}>
                                                        <td>{++rank}</td>
                                                        <td>{listingContact.listing.id}</td>
                                                        <td>{listingContact.listing.make}</td>
                                                        <td>€ {listingContact.listing.price},-</td>
                                                        <td>{listingContact.listing.mileage} KM</td>
                                                        <td>{listingContact.contacts}</td>
                                                    </tr>
                                                            )
                                                    }
                                                  </tbody>
                                         </Table>
                                </Card.Body>
                        </Card>
                    <br></br>
                    <p hidden>{rank=0}</p>
                 </tr>
                                                 )
                    );
            }
}

