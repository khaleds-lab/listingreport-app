import React from 'react';
import './App.css';

import {Container, Row, Col} from 'react-bootstrap';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import NavigationBar from './components/NavigationBar';
import Welcome from './components/Welcome';
import AveragePriceMostContactedListing from './components/AveragePriceMostContactedListing';
import TopMostContactedListing from './components/TopMostContactedListing';
import SellingPricePerSeller from './components/SellingPricePerSeller';
import MakePercentDistribution from './components/MakePercentDistribution';
import Footer from './components/Footer';
import FileAction from './components/FileAction';

function App() {
  const marginTop = {
    marginTop:"20px"
  };

  return (
    <Router>
        <NavigationBar/>
        <Container>
            <Row>
                <Col lg={12} style={marginTop}>
                    <Switch>
                        <Route path="/" exact component={Welcome}/>
                        <Route path="/averageSellingPrice" exact component={SellingPricePerSeller}/>
                        <Route path="/getMakePercent" exact component={MakePercentDistribution}/>
                        <Route path="/averagePrice" exact component={AveragePriceMostContactedListing}/>
                        <Route path="/topFiveMostContacted" exact component={TopMostContactedListing}/>
                        <Route path="/fileUpload" exact component={FileAction}/>
                    </Switch>
                </Col>
            </Row>
        </Container>
        <Footer/>
    </Router>
  );
}

export default App;
