package com.autoscout24.listing.service;

import com.autoscout24.listing.entity.Contact;
import com.autoscout24.listing.service.util.CSVParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContactService {

    private static final Logger logger = LoggerFactory.getLogger(ContactService.class);

    String contactsFile = "src/main/resources/contacts.csv";


    protected Map<Integer, Integer> getTopFiveMostContactedListingIdsPerMonth(int month) {
        Map<Integer, Integer> listingIdsWithTotalAmountOfContacts = this.getTotalAmountOfContactsPerListingIdsByMonth(month);

        return listingIdsWithTotalAmountOfContacts
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(5)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    protected Map<Integer, Integer> getTotalContactedListingIdsSorted() {
        Map<Integer, Integer> listingIdsWithTotalAmountOfContacts = this.getTotalAmountOfContactsPerListingIds();

        return listingIdsWithTotalAmountOfContacts
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    private Map<Integer, Integer> getTotalAmountOfContactsPerListingIds() {
        Map<Integer, Integer> listingIdsTotalWithAmountOfContacts = new HashMap<>();
        this.getAllContactDatesPerListingId().forEach((integer, dates) -> listingIdsTotalWithAmountOfContacts.put(integer, dates.size()));
        return listingIdsTotalWithAmountOfContacts;
    }

    private Map<Integer, Integer> getTotalAmountOfContactsPerListingIdsByMonth(int month) {
        Map<Integer, Integer> listingIdsWithTotalAmountOfContacts = new HashMap<>();

        this.getAllContactDatesPerListingIdByMonth(month)
                .forEach((integer, dates) -> {
                    if (dates.size() > 0) {
                        listingIdsWithTotalAmountOfContacts.put(integer, dates.size());
                    }
                });
        return listingIdsWithTotalAmountOfContacts;
    }

    private Map<Integer, List<Date>> getAllContactDatesPerListingIdByMonth(int month) {
        Map<Integer, List<Date>> contactsPerListingsIdByMonth = this.getAllContactDatesPerListingId();
        contactsPerListingsIdByMonth.forEach((integer, dates) -> dates.removeIf(date -> date.getMonth() != month));
        return contactsPerListingsIdByMonth;
    }

    private Map<Integer, List<Date>> getAllContactDatesPerListingId() {
        Map<Integer, List<Date>> contactsPerListingId = new HashMap<>();

        for (Contact contact : this.getContacts()) {
            if (contactsPerListingId.containsKey(contact.getListingId())) {
                List<Date> dates = contactsPerListingId.get(contact.getListingId());
                dates.add(contact.getContactDate());
                contactsPerListingId.put(contact.getListingId(), dates);

            } else {
                contactsPerListingId.put(contact.getListingId(), new ArrayList<>(Arrays.asList(contact.getContactDate())));
            }
        }
        return contactsPerListingId;
    }

    List<Contact> getContacts() {
        return Objects.requireNonNull(this.parseContactsFile());
    }

    private List<Contact> parseContactsFile() {
        try {
            return CSVParser.parseFileToList(contactsFile, Contact.class);
        } catch (FileNotFoundException e) {
            logger.info(e.getMessage());
        }
        return null;
    }

    public void setContactsFile(String contactsFile) {
        this.contactsFile = contactsFile;
    }

}
