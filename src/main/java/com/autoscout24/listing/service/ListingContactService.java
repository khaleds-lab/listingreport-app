package com.autoscout24.listing.service;

import com.autoscout24.listing.entity.Listing;
import com.autoscout24.listing.entity.ListingContact;
import com.autoscout24.listing.entity.MakePercentage;
import com.autoscout24.listing.entity.SellerTypeSellerPrice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
public class ListingContactService {

    ListingService listingService;

    ContactService contactService;

    private final Path root = Paths.get("src/main/resources/uploads");


    @Inject
    public ListingContactService(ListingService listingService, ContactService contactService) {
        this.listingService = listingService;
        this.contactService = contactService;
    }

    public ResponseEntity<String> uploadListingsFile(MultipartFile file) throws IOException {
        Consumer<String> consumer = (String files) -> listingService.setListingsFile(files);
        return this.upload(file, consumer);
    }

    public ResponseEntity<String> uploadContactsFile(MultipartFile file) throws IOException {
        Consumer<String> consumer = (String files) -> contactService.setContactsFile(files);
        return this.upload(file, consumer);
    }

    public List<SellerTypeSellerPrice> getAverageSellingPrice() {
        Map<String, Integer> averageSellingPricePerSellerType = listingService.getAverageSellingPricePerSellerType();
        List<SellerTypeSellerPrice> sellerPrices = new ArrayList<>();
        averageSellingPricePerSellerType.forEach((s, integer) -> sellerPrices.add(new SellerTypeSellerPrice(s, integer)));
        return sellerPrices;
    }

    public List<MakePercentage> getPercentageOfMakes() {
        Map<String, Integer> percentageOfCarsByMake = listingService.getPercentageOfCarsByMake();
        List<MakePercentage> makePercentages = new ArrayList<>();
        percentageOfCarsByMake.forEach((s, integer) -> makePercentages.add(new MakePercentage(s, integer)));
        return makePercentages;
    }

    public int getAveragePriceOfThirtyPercentMostContactedListings() {
        List<ListingContact> totalContactedListings = this.getMostContactedListingsSorted();
        int size = totalContactedListings.size();
        int amountToGetByPercentage = (int) (0.3 * size);

        List<ListingContact> thirtyPercentMostContactedListings = totalContactedListings
                .stream()
                .limit(amountToGetByPercentage)
                .collect(Collectors.toList());

        return (int) thirtyPercentMostContactedListings
                .stream()
                .mapToInt(listing -> listing.getListing().getPrice())
                .average().getAsDouble();
    }

    public List<List<ListingContact>> getAllContactsPerListingForYear() {
        List<List<ListingContact>> listOfTopFiveMostContactedListingsPerMonth = new ArrayList<>();
        for (int i = 0; i < 12; i++) {

            List<ListingContact> listingContacts = this.getTopFiveMostContactedListingsByMonth(i);

            if (!listingContacts.isEmpty()) {
                listOfTopFiveMostContactedListingsPerMonth.add(listingContacts);
            }
        }
        return listOfTopFiveMostContactedListingsPerMonth;
    }

    private List<ListingContact> getMostContactedListingsSorted() {
        List<ListingContact> totalContactedListings = new ArrayList<>();

        Map<Integer, Integer> totalContactedListingIds = contactService.getTotalContactedListingIdsSorted();

        totalContactedListingIds
                .forEach((listingId, contacts) -> totalContactedListings.add(new ListingContact(this.getListingById(listingId), contacts)));
        return totalContactedListings;
    }

    private List<ListingContact> getTopFiveMostContactedListingsByMonth(int month) {
        List<ListingContact> topFiveContactedListings = new ArrayList<>();

        Map<Integer, Integer> mostContactedListingIdsForMonth = contactService.getTopFiveMostContactedListingIdsPerMonth(month);

        mostContactedListingIdsForMonth
                .forEach((listingId, contacts) -> topFiveContactedListings.add(new ListingContact(this.getListingById(listingId), contacts)));
        return topFiveContactedListings;
    }

    private Listing getListingById(int listingId) {
        return this.listingService.getListings()
                .stream()
                .filter(listing -> listing.getId() == listingId)
                .findFirst()
                .orElse(null);
    }

    private ResponseEntity<String> upload(MultipartFile file, Consumer<String> consumer) throws IOException {
        if (file.getContentType().endsWith("csv")) {

            if (!Files.exists(root)) {
                Files.createDirectory(root);
            }
            Path uploadedFile = root.resolve(file.getOriginalFilename());

            if (!Files.exists(uploadedFile)) {
                Files.copy(file.getInputStream(), uploadedFile);
            }
            consumer.accept(uploadedFile.toString());
            return ResponseEntity.status(HttpStatus.OK).body("File Uploaded Successfully");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("File Upload failed!");
    }

    public ResponseEntity<String> deleteAllUploadedFiles() {
        if (!FileSystemUtils.deleteRecursively(root.toFile())) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Files were not Found!");
        }
        return ResponseEntity.status(HttpStatus.OK).body("Files Deleted!");
    }

}
