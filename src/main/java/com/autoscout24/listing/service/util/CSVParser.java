package com.autoscout24.listing.service.util;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class CSVParser {

    public static <T> List<T> parseFileToList(final String file, final Class<T> classType) throws FileNotFoundException {
        if (file == null || file.isEmpty()) {
            throw new IllegalArgumentException("File directory is not found");
        }
        if (classType == null) {
            throw new IllegalArgumentException("The type of the bean Class is null");
        }
        return new CsvToBeanBuilder(new FileReader(file))
                .withType(classType)
                .build()
                .parse();
    }

}
