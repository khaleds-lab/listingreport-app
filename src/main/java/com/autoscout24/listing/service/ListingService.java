package com.autoscout24.listing.service;

import com.autoscout24.listing.entity.Listing;
import com.autoscout24.listing.service.util.CSVParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ListingService {

    private static final Logger logger = LoggerFactory.getLogger(ListingService.class);

    String listingsFile = "src/main/resources/listings.csv";

    protected Map<String, Integer> getAverageSellingPricePerSellerType() {

        Map<String, Integer> averageSellingPrice = new HashMap<>();

        for (Listing seller : this.getListings()) {
            if (averageSellingPrice.containsKey(seller.getSellerType())) {
                averageSellingPrice.put(seller.getSellerType(), averageSellingPrice.get(seller.getSellerType()) + seller.getPrice());
            } else {
                averageSellingPrice.put(seller.getSellerType(), seller.getPrice());
            }
        }

        List<String> sellerTypeList = this.getListings().stream().map(Listing::getSellerType).collect(Collectors.toList());

        averageSellingPrice.forEach((s, integer) -> {
            averageSellingPrice.put(s, integer / (Collections.frequency(sellerTypeList, s)));
        });

        return averageSellingPrice;
    }

    protected Map<String, Integer> getPercentageOfCarsByMake() {

        Map<String, Integer> percentageOfCars = new HashMap<>();

        for (Listing make : this.getListings()) {
            if (percentageOfCars.containsKey(make.getMake())) {
                percentageOfCars.put(make.getMake(), percentageOfCars.get(make.getMake()) + 1);
            } else {
                percentageOfCars.put(make.getMake(), 1);
            }
        }
        double makeSize = this.getListings().size();
        percentageOfCars.forEach((s, integer) -> {
            percentageOfCars.put(s, (int) ((integer / makeSize) * 100));
        });
        return percentageOfCars
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }


    List<Listing> getListings() {
        return Objects.requireNonNull(this.parseListingsFile());
    }

    private List<Listing> parseListingsFile() {
        try {
            return CSVParser.parseFileToList(listingsFile, Listing.class);
        } catch (FileNotFoundException e) {
            logger.info(e.getMessage());
        }
        return null;
    }

    void setListingsFile(String listingsFile) {
        this.listingsFile = listingsFile;
    }

}
