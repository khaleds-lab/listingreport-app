package com.autoscout24.listing.entity;

public class ListingContact {

    private Listing listing;

    private int contacts;

    public ListingContact(Listing listing, int contacts) {
        this.listing = listing;
        this.contacts = contacts;
    }

    public Listing getListing() {
        return listing;
    }

    public int getContacts() {
        return contacts;
    }

    @Override
    public String toString() {
        return "ListingContact{" +
                "listing=" + listing +
                ", contacts=" + contacts +
                '}';
    }
}
