package com.autoscout24.listing.entity;

import com.opencsv.bean.CsvBindByName;

import java.util.Date;
import java.util.Objects;

public class Contact {

    @CsvBindByName(column = "listing_id", required = true)
    private int listingId;

    @CsvBindByName(column = "contact_date", required = true)
    private long contactDate;

    public int getListingId() {
        return this.listingId;
    }

    public Date getContactDate() {
        return new Date(this.contactDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return this.listingId == contact.listingId &&
                this.getContactDate() == contact.getContactDate();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.listingId, this.getContactDate());
    }

    @Override
    public String toString() {
        return "Contact{" +
                "listingId=" + this.listingId +
                ", contactDate=" + this.getContactDate() +
                '}';
    }

}
