package com.autoscout24.listing.entity;

import java.util.Objects;

public class SellerTypeSellerPrice {

    private String sellerType;

    private int sellerPrice;

    public SellerTypeSellerPrice(String sellerType, int sellerPrice) {
        this.sellerType = sellerType;
        this.sellerPrice = sellerPrice;
    }

    public String getSellerType() {
        return sellerType;
    }

    public int getSellerPrice() {
        return sellerPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SellerTypeSellerPrice that = (SellerTypeSellerPrice) o;
        return sellerPrice == that.sellerPrice &&
                sellerType.equals(that.sellerType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sellerType, sellerPrice);
    }

    @Override
    public String toString() {
        return "SellerTypeSellerPrice{" +
                "sellerType='" + sellerType + '\'' +
                ", sellerPrice=" + sellerPrice +
                '}';
    }

}
