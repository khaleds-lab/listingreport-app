package com.autoscout24.listing.entity;

import java.util.Objects;

public class MakePercentage {

    private String make;

    private int percentage;

    public MakePercentage(String make, int percentage) {
        this.make = make;
        this.percentage = percentage;
    }

    public String getMake() {
        return make;
    }

    public int getPercentage() {
        return percentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MakePercentage that = (MakePercentage) o;
        return percentage == that.percentage &&
                make.equals(that.make);
    }

    @Override
    public int hashCode() {
        return Objects.hash(make, percentage);
    }

    @Override
    public String toString() {
        return "MakePercentage{" +
                "make='" + make + '\'' +
                ", percentage=" + percentage +
                '}';
    }
}
