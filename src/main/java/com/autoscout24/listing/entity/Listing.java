package com.autoscout24.listing.entity;

import com.opencsv.bean.CsvBindByName;

import java.util.Objects;

public class Listing {

    @CsvBindByName(column = "id", required = true)
    private int id;

    @CsvBindByName(column = "make", required = true)
    private String make;

    @CsvBindByName(column = "price", required = true)
    private int price;

    @CsvBindByName(column = "mileage", required = true)
    private int mileage;

    @CsvBindByName(column = "seller_type", required = true)
    private String sellerType;

    public int getId() {
        return this.id;
    }

    public String getMake() {
        return this.make;
    }

    public int getPrice() {
        return this.price;
    }

    public int getMileage() {
        return this.mileage;
    }

    public String getSellerType() {
        return this.sellerType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Listing listing = (Listing) o;
        return this.id == listing.id &&
                this.price == listing.price &&
                this.mileage == listing.mileage &&
                this.make.equals(listing.make) &&
                this.sellerType.equals(listing.sellerType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.make, this.price, this.mileage, this.sellerType);
    }

    @Override
    public String toString() {
        return "Listing{" +
                "id=" + this.id +
                ", make='" + this.make + '\'' +
                ", price=" + this.price +
                ", mileage=" + this.mileage +
                ", sellerType='" + this.sellerType + '\'' +
                '}';
    }

}
