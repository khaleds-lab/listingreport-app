package com.autoscout24.listing.rest;

import com.autoscout24.listing.entity.ListingContact;
import com.autoscout24.listing.entity.MakePercentage;
import com.autoscout24.listing.entity.SellerTypeSellerPrice;
import com.autoscout24.listing.service.ListingContactService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * Listing Report
 *
 * @author Khaled Ahmed
 */
@RestController
@CrossOrigin(origins = "*")
public class ListingReportController {

    ListingContactService service;

    @Inject
    public ListingReportController(ListingContactService service) {
        this.service = service;
    }

    /**
     * Uploads new Listings files
     *
     * @return Http Status code of action
     */
    @PostMapping("/uploadListings")
    public ResponseEntity<String> uploadListingsFile(@RequestParam("file") MultipartFile file) {
        try {
            return service.uploadListingsFile(Objects.requireNonNull(file));
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Failed to Upload the File");
        }
    }

    /**
     * Uploads new Contacts files
     *
     * @return Http Status code of action
     */
    @PostMapping("/uploadContacts")
    public ResponseEntity<String> uploadContactsFile(@RequestParam("file") MultipartFile file) {
        try {
            return service.uploadContactsFile(Objects.requireNonNull(file));
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Failed to Upload the File");
        }
    }

    /**
     * Gets the average selling price of each seller type (private, dealer, other)
     *
     * @return a list of each seller type along with its average selling price
     */
    @GetMapping("/averageSellingPrice")
    public List<SellerTypeSellerPrice> getAverageSellingPricePerSeller() {
        return service.getAverageSellingPrice();
    }

    /**
     * Gets the percentual distribution of cars by Make (brand)
     *
     * @return a list of the car Make along with its percentage of distribution
     */
    @GetMapping("/getMakePercent")
    public List<MakePercentage> getMakePercent() {
        return service.getPercentageOfMakes();
    }

    /**
     * Gets the average price of the 30% most contacted listings
     *
     * @return an int that holds the average value
     */
    @GetMapping("/averagePrice")
    public int getAveragePriceOfThirtyPercentMostContactedListings() {
        return service.getAveragePriceOfThirtyPercentMostContactedListings();
    }

    /**
     * Gets Top 5 most contacted listings per Month
     *
     * @return a list of list containing each month along with its top listings contacted
     */
    @GetMapping("/topFiveMostContacted")
    public List<List<ListingContact>> getTopFiveMostContactedListingPerMonth() {
        return service.getAllContactsPerListingForYear();
    }

    /**
     * Deletes all uploaded files
     *
     * @return Http Status code of action
     */
    @GetMapping("/deleteAll")
    public ResponseEntity<String> deleteAll() {
        return service.deleteAllUploadedFiles();
    }

}
