package com.autoscout24.listing.service.util;

import com.autoscout24.listing.entity.Listing;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVParserTest {

    @Test
    void testCSVParse_ok() throws FileNotFoundException {
        //prepare
        String listingsFile = "src/main/resources/listings.csv";
        int numberOfListings = 300;

        //test
        List<Listing> listings = CSVParser.parseFileToList(listingsFile, Listing.class);

        //assert
        assertEquals(listings.size(), numberOfListings);
    }

    @Test
    void testParseNullFile_error() {
        assertThrows(IllegalArgumentException.class,
                () -> CSVParser.parseFileToList(null, Listing.class));
    }

    @Test
    void testParseEmptyFile_error() {
        assertThrows(IllegalArgumentException.class,
                () -> CSVParser.parseFileToList("", Listing.class));
    }

    @Test
    void testParseNullClass_error() {
        //prepare
        String listingsFile = "src/main/resources/listings.csv";

        //test & assert
        assertThrows(IllegalArgumentException.class,
                () -> CSVParser.parseFileToList(listingsFile, null));
    }
}