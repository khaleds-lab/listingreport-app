package com.autoscout24.listing.rest;

import com.autoscout24.listing.entity.ListingContact;
import com.autoscout24.listing.entity.MakePercentage;
import com.autoscout24.listing.entity.SellerTypeSellerPrice;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@SpringBootTest
class ListingReportControllerTest {

    @Inject
    ListingReportController controller;

    @Test
    void testGetAverageSellingPricePerSeller() {
        //prepare
        List<SellerTypeSellerPrice> averageSellingPricePerSeller = new ArrayList<>();
        averageSellingPricePerSeller.add(new SellerTypeSellerPrice("private", 26080));
        averageSellingPricePerSeller.add(new SellerTypeSellerPrice("other", 25317));
        averageSellingPricePerSeller.add(new SellerTypeSellerPrice("dealer", 25037));

        //test
        List<SellerTypeSellerPrice> averageSellingPricePerSellerResult = controller.getAverageSellingPricePerSeller();

        //assert
        assertEquals(averageSellingPricePerSeller, averageSellingPricePerSellerResult);
    }

    @Test
    void testGetMakePercent() {
        //prepare
        List<MakePercentage> percentageOfMake = new ArrayList<>();
        percentageOfMake.add(new MakePercentage("Toyota", 16));
        percentageOfMake.add(new MakePercentage("Mercedes-Benz", 16));
        percentageOfMake.add(new MakePercentage("Renault", 14));
        percentageOfMake.add(new MakePercentage("Audi", 14));
        percentageOfMake.add(new MakePercentage("Mazda", 13));
        percentageOfMake.add(new MakePercentage("VW", 10));
        percentageOfMake.add(new MakePercentage("Fiat", 9));
        percentageOfMake.add(new MakePercentage("BWM", 7));

        //test
        List<MakePercentage> percentageOfMakeResult = controller.getMakePercent();

        //assert
        assertEquals(percentageOfMake, percentageOfMakeResult);
    }

    @Test
    void testGetAveragePriceOfThirtyPercentMostContactedListings() {
        //prepare
        int averagePriceOfThirtyPercentMostContactedListings = 24638;

        //test
        int averagePriceOfThirtyPercentMostContactedListingsResult = controller.getAveragePriceOfThirtyPercentMostContactedListings();

        //assert
        assertEquals(averagePriceOfThirtyPercentMostContactedListings, averagePriceOfThirtyPercentMostContactedListingsResult);
    }

    @Test
    void testGetTopFiveMostContactedListingPerMonth() {
        //prepare
        int sizeOfListingsPerMonth = 5;

        //test
        List<List<ListingContact>> topFiveMostContactedListingPerMonthForYear = controller.getTopFiveMostContactedListingPerMonth();
        List<ListingContact> topFiveMostContactedListingForMonth = topFiveMostContactedListingPerMonthForYear.get(0);

        //assert
        assertEquals(topFiveMostContactedListingForMonth.size(), sizeOfListingsPerMonth);
    }

}