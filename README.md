# Listing Report App

**Getting started**

Go into the folder `src/main/resources/webapp/reactjs` and run `npm install`. Then to start the server, run `npm start`. Now your frontend is up and running!

<h2></h2>


For the Backend, run `mvn spring-boot:run` within the project directory.

